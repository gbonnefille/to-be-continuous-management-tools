#!/bin/sh

if [ "$#" != 1 ]
then
  echo >&2 "Expect JSON"
  exit 1
fi
JSON=$1

STAGES="build test package_build package_test infra deploy acceptance publish infra_prod production"

cat <<EOF
<!DOCTYPE html>
<html>
<style>
table, th, td {
  border:1px solid black;
}
</style>
<body>
EOF
echo "<!-- Found stages"
jq -r '.[] | .stages | .[]' <$JSON | sort -u
echo "-->"
echo '<table>'
# Header
echo "<tr><th></th>"
echo $STAGES | tr " " "\n" | xargs -i echo "<th>{}</th>"
echo "</tr>"
# Data
STAGES_JQ=`echo $STAGES | tr " " "\n" | xargs -i echo -n '"{}"' | sed 's/""/","/g'`
cat $JSON | \
jq '[.[] | .stages as $stages | { "name": .name, "avatar": .avatar, "stages": [ ['$STAGES_JQ'] | map({ "item": ., "isin": (. | IN($stages[]))}) ]}]' | \
jq -r '.[] | "<tr><td><img src=\"\(.avatar)\" height=\"15\"/>\(.name)</td>\(.stages[] | map(if .isin then "<td>x</td>" else "<td></td>" end) | join(""))</tr>"'
echo '</table>'
echo "</body>"
