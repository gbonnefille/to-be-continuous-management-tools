#!/bin/sh

projects=`curl -s https://gitlab.com/api/v4/groups/to-be-continuous | jq -r '.projects[].path' `

getdefaultbranch() {
  project=$(echo $1 | sed "s/\//%2F/g")
  curl -s "https://gitlab.com/api/v4/projects/$project" | jq -r '.default_branch // "master"'
}

# Separator for data
SEP=""
echo '['
for project in $projects
do
  echo >&2 "Processing $project..."
  branch=`getdefaultbranch to-be-continuous/$project`
  echo >&2 "on branch $branch..."
  echo "$SEP{"
  echo "\"name\": \"$project\","
  echo "\"avatar\": \"https://gitlab.com/to-be-continuous/$project/-/avatar\","
  gitlabciyml=`curl -s https://gitlab.com/to-be-continuous/$project/-/raw/$branch/README.md | sed -n -e "s@^ *file: '/\(.*\)'@\1@p" | head -1`
  echo "\"template\": \"$gitlabciyml\","
  stages=`curl -s https://gitlab.com/to-be-continuous/$project/-/raw/$branch/$gitlabciyml | sed -n -e "s/^ *stage: \(.*\) *.*/\1/p" | sort -u`
  echo "\"stages\": `echo $stages | tr ' ' '\n' | jq -R '.' | jq -s '.'`"
  echo '}'
  # Next data will need a separator
  SEP=","
done
echo ']'
